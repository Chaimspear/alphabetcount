﻿using AlphabetCount.Data;
using System;
using System.Text;
using System.Text.RegularExpressions;
using StringUtlity.Core.Classes;

namespace AlphabetCount
{
     

    class Program
    { 
        static void Main(string[] args)
        {
            Console.WriteLine("Enter some text:");

            var line = Console.ReadLine();

            var alphabetCountUtility = new AlphabetCountUtility();

            var AlphabetCharLst = alphabetCountUtility.ParseStringForAlphabetCharacters(line);
            var NotfirstRowFlag = false;
            var AlphabetRowStr = string.Empty;
            var CountRowStr = string.Empty;
            var totalCharsCount = 0;

            foreach (var strPattern in AlphabetCharLst)
            {
                if (!NotfirstRowFlag) //only for the first record do we need to specifically put the left table 'wall'
                {
                    NotfirstRowFlag = true;
                    AlphabetRowStr = "|";
                    CountRowStr = "|";
                }

                totalCharsCount += strPattern.PatternCount; //will be used below
                var patternCountStr = strPattern.PatternCount.ToString();

                //include extra spaces in case the count is more than a single digit
                AlphabetRowStr += " " + strPattern.Pattern + Regex.Replace(patternCountStr, "[0-9]", " ") + "|";
                CountRowStr += " " + patternCountStr + " |"; //the right table 'wall'
            }

            // ********* ReplaceAllChars is an Extension method ********* 

            //width of table line will be the same as the table row below
            var tableLine = AlphabetRowStr.ReplaceAllChars("-");

            Console.WriteLine(tableLine);
            Console.WriteLine(AlphabetRowStr);
            Console.WriteLine(tableLine);
            Console.WriteLine(CountRowStr);
            Console.WriteLine(tableLine);
            Console.WriteLine("");
            Console.WriteLine("The text has been processed.");
            Console.WriteLine($"Total letters counted: {totalCharsCount}");
             
        }
    }
}
