﻿using StringUtlity.Core.Classes;
using System.Collections.Generic;

namespace AlphabetCount.Data
{
    public interface IAlphabetCountUtility //using interface in case in theory project is used with .net core.
    {
        IEnumerable<StringPattern> ParseStringForAlphabetCharacters(string InputStr);
    }
}
