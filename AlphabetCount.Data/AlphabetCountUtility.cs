﻿using StringUtlity.Core.Classes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AlphabetCount.Data
{
    public class AlphabetCountUtility : IAlphabetCountUtility
    {
        public IEnumerable<StringPattern> ParseStringForAlphabetCharacters(string InputStr)
        { 
            var onlyAlphabetChrArr = new String(InputStr.ToUpper().Where(Char.IsLetter).ToArray()).ToCharArray();

            //group all the characters, with their counts 
            var onlyAlphabetQry = from alph in onlyAlphabetChrArr
                                  group alph by alph into alphGrp
                                  orderby alphGrp.Key
                                  select new StringPattern
                                  {
                                      Pattern = alphGrp.Key.ToString(),
                                      PatternCount = alphGrp.Count()
                                  };

            return onlyAlphabetQry; 
        }
    }
}
