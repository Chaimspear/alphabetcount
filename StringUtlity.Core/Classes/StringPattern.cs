﻿using System;

namespace StringUtlity.Core.Classes
{
    public class StringPattern
    {
        public string Pattern { get; set; }
        public int PatternCount { get; set; }
    }
}
