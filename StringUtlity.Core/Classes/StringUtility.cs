﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StringUtlity.Core.Classes
{
    public static class StringUtility
    {
        public static string ReplaceAllChars(this String str, string replaceWith)
        {
            StringBuilder sb = new StringBuilder(str.Length);
            for (int i = 0; i < str.Length; i++)
            {
                sb.Append(replaceWith);
            }

            return sb.ToString();

        }
    }
}
